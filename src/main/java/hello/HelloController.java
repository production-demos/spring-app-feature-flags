package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import no.finn.unleash.util.*;
import no.finn.unleash.*;


@RestController
public class HelloController {

    private static UnleashConfig config = UnleashConfig.builder()
            .appName("spring-app-feature-flags")
            .instanceId("YWg85xwgz7pE2pHR1W4s")
            .unleashAPI("https://gitlab.com/api/v4/feature_flags/unleash/13007250")
            .build();

    private static Unleash unleash = new DefaultUnleash(config);

    @RequestMapping("/")
    public String index() {
        String style = "<style type='text/css' media='screen'>";
        style += "body { background-color: #6b4fbb; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }";
        style += "</style>";
        
        String message = "Hello from GitLab!!!";

        if(unleash.isEnabled("tanuki-logo")) {
            message += "<p><center><img src='https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png' alt='Tanuki' width='250'></center>";
        } 

        String body = "<body>" + message + "</body>";

        return style + body; 
    }

}